---
title: Game Title GDD
author: authorname
date: 2019-08-17
---

Game Title
==============

<br />

Table of Contents
--------------
[TOC]

Introduction
--------------
### Elevator Pitch #######
(This section contains what you'd say if you had 15 seconds to describe your game to Miyamoto. It should be a couple of sentences, and only describe the essence of your game.)

### This Document #######
(Outline the point of the document here. If you have other documents with more detail, you might like to list them here. If your developers can use this document and get exactly what you intend, you should say so.)

Game Description
--------------
This section describes the general game ideas and design. Reading this should give a full idea of many of the gameplay elements, but maybe not the specifics of their implementation or all interactions between system, etc. The following (...) system sections go into further detail.

### Inspirations #######
(Give some context for the game)

### Concept #######
(Genre and general idea. Why it's interesting)

### Background and Story #######
(If there is one)

### Tone and Style #######
(The manner that the background and story is utilized. What kind of feeling are we trying to present)

### Objective #######
(Onto actual game stuff. What is the goal of the game?)

### Gameplay #######
(What playing the game is actually like. How does stuff work. What are the main loops?)

### Controls and Interface #######
(What does the game look and play like? How do you do stuff? How do you see stuff happen?)

The X System
--------------
This section describes the X System in detail

The Y System
--------------
This section describes the Y System in detail

Walkthrough
--------------
(This section might come a bit later in development, when you can visualise how things will actually go)
The purpose of this section is to describe in detail exactly what happens during the first hour or so of playing the game. This will describe what the game looks like, what's happening, what the player is doing, and why the player is doing it. This will hopefully give a good feel of what the whole game will be like, as well as allow a reviewer to absolutely understand the game quickly without having to become an expert (though note the philosophical argument of tainting the real experience, bla bla). Additionally, this section will incidentally outline the tutorial, and so be an implementation guide for that.

Assets
--------------
This section attempts to describe everything that needs to be done to complete making the game.

### Code #######

#### Save Data #######

### Story #######

### Levels/Quests #######

### UI #######

### Art #######

### Music & Sound #######
